#include "block.hpp"

#define BLOCK_SIZE 2

using namespace std;

Block::Block(int x, int y) : Form(x, y) {
    formTemplate = vector< vector<bool> > (BLOCK_SIZE, vector<bool>(BLOCK_SIZE));
    defineTemplate();
}

void Block::defineTemplate() {
    for (auto &&row : formTemplate) {
        for (auto &&element : row) {
            element = true;
        }
    }
}