#ifndef _GUN_HPP_
#define _GUN_HPP_

#include "form.hpp"

class Gun : public Form {
    public:
        Gun(int x, int y);
        void defineTemplate();
};

#endif