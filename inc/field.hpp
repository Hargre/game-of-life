#ifndef _FIELD_HPP_
#define _FIELD_HPP_

#include "form.hpp"
#include <vector>

class Field {
    private:
        std::vector< std::vector<bool> > grid;
    public:
        Field();
        std::vector< std::vector<bool> > getGrid();
        void setGrid(std::vector< std::vector<bool> > grid);
        void applyForm(Form &form);
        void printGrid();
};

#endif