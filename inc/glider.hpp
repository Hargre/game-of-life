#ifndef _GLIDER_HPP_
#define _GLIDER_HPP_

#include "form.hpp"

class Glider : public Form {
    public:
        Glider(int x, int y);
        void defineTemplate();
};

#endif