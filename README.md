Simple implementation of Conway's Game of Life, working with a basic number of forms.

## Instalation and execution

* Clone the repository;
* Inside it, compile with **make**.
* After compiling, execute with **make run**.
* If anything goes wrong, try cleaning the object and binary files with **make clean**, then compile again.