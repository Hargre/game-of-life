#ifndef _LIFE_HPP_
#define _LIFE_HPP_

#include "field.hpp"

class Life {
    private:
        Field *field;
        int countNeighbours(int x, int y);
    public:
        Life();
        Field *getField();
        void applyRules();
};

#endif