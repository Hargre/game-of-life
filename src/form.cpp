#include "form.hpp"

using namespace std;

Form::Form(int x, int y) {
    startingCoord = pair<int, int> (x, y);
}

int Form::getX() {
    return startingCoord.first;
}

int Form::getY() {
    return startingCoord.second;
}

vector< vector<bool> > Form::getTemplate() {
    return formTemplate;
}