#ifndef _BLOCK_HPP_
#define _BLOCK_HPP_

#include "form.hpp"

class Block : public Form {
    public:
        Block(int x, int y);
        void defineTemplate();
};

#endif