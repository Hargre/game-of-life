#include "field.hpp"
#include "life.hpp"
#include "block.hpp"
#include "blinker.hpp"
#include "glider.hpp"
#include "gun.hpp"
#include <iostream>
#include <unistd.h>

using namespace std;

int main() {
    int option;
    int x, y;
    int generations;
    Life life;

    cout << "Welcome to Conway's game of life! Select an option:" << endl;
    cout << "1 - Block" << endl;
    cout << "2 - Blinker" << endl;
    cout << "3 - Glider" << endl;
    cout << "4 - Gosper Glider Gun" << endl;
    
    cin >> option;

    while (option < 1 || option > 4) {
        cout << "Invalid option! Please try again:" << endl;
        cin >> option;
    }

    cout << "Choose the starting coordinates for your form!" << endl;
    cout << "x: ";
    cin >> x;
    cout << "y: ";
    cin >> y;
    cout << endl;

    cout << "For how many generations do you want the game to run?" << endl;
    cin >> generations;

    switch (option) {
        case 1: {
            Block block(x, y);
            life.getField()->applyForm(block);
            break;
        }
        case 2: {
            Blinker blinker(x, y);
            life.getField()->applyForm(blinker);
            break;
        }
        case 3: {
            Glider glider(x, y);
            life.getField()->applyForm(glider);
            break;
        }
        case 4: {
            Gun gun(x, y);
            life.getField()->applyForm(gun);
            break;
        }
        default:
            break;
    }
    
    for (int i = 0; i < generations; i++) {
        life.applyRules();
        life.getField()->printGrid();
        usleep(150000); 
    }

    return 0;
}