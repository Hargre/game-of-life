#include <iostream>
#include "field.hpp"

#define MAP_H 24
#define MAP_W 40

using namespace std;

Field::Field() {
    grid = vector< vector<bool> > (MAP_H, vector<bool>(MAP_W, false));
}

vector< vector<bool> > Field::getGrid() {
    return grid;
}

void Field::applyForm(Form &form) {
    int x = form.getX();
    int y = form.getY();
    auto formTemplate = form.getTemplate();

    for (size_t i = 0; i < formTemplate.size(); i++, x++) {
        for (size_t j = 0; j < formTemplate[i].size(); j++, y++) {
            this->grid[x][y] = formTemplate[i][j];
        }
        y = form.getY();
    }

    printGrid();
    cout << endl;
}

void Field::setGrid(vector < vector<bool> > grid) {
    this->grid = grid;
}

void Field::printGrid() {
    for (auto row : grid) {
        for (auto element : row) {
            if (element) {
                cout << 'o' << ' ';
            } else {
                cout << "  ";
            }
        }
        cout << endl;
    }
    cout << endl;
}