#include "life.hpp"
#include <iostream>

Life::Life() {
    this->field = new Field();
}

Field *Life::getField() {
    return field;
}

void Life::applyRules() {
    auto temp = field->getGrid();
    for (size_t i = 1; i < temp.size() - 1; i++) {
        for (size_t j = 1; j < temp[i].size() - 1; j++) {
            int neighbours = countNeighbours(i, j);
            if (!field->getGrid()[i][j]) {
                if (neighbours == 3) {
                    temp[i][j] = true;
                } 
            } else {
                if (neighbours < 2) {
                    temp[i][j] = false;
                } else if (neighbours > 3) {
                    temp[i][j] = false;
                }
            }
        }
    }

    field->setGrid(temp);
}

int Life::countNeighbours(int x, int y) {
    auto grid = field->getGrid();
    int neighbours = 0;

    if (grid[x][y - 1]) {
        neighbours++;
    }
    if (grid[x][y + 1]) {
        neighbours++;
    }
    if (grid[x - 1][y]) {
        neighbours++;
    }
    if (grid[x + 1][y]) {
        neighbours++;
    }
    if (grid[x - 1][y - 1]) {
        neighbours++;
    }
    if (grid[x - 1][y + 1]) {
        neighbours++;
    }
    if (grid[x + 1][y - 1]) {
        neighbours++;
    }
    if (grid[x + 1][y + 1]) {
        neighbours++;
    }

    return neighbours;
}