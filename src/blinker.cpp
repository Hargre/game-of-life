#include "blinker.hpp"

#define BLINKER_WIDTH 1
#define BLINKER_HEIGHT 3

using namespace std;

Blinker::Blinker(int x, int y) : Form(x, y) {
    formTemplate = vector< vector<bool> > (BLINKER_WIDTH, vector<bool>(BLINKER_HEIGHT));
    defineTemplate();
}

void Blinker::defineTemplate() {
    for (auto &&row : formTemplate) {
        for (auto &&element : row) {
            element = true;
        }
    }
}
