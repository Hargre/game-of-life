#include "glider.hpp"

#define GLIDER_SIZE 3

using namespace std;

Glider::Glider(int x, int y) : Form(x, y) {
    formTemplate = vector< vector<bool> > (GLIDER_SIZE, vector<bool>(GLIDER_SIZE, false));
    defineTemplate();
}

void Glider::defineTemplate() {
    formTemplate[0][1] = true;
    formTemplate[1][2] = true;
    for (int i = 0; i < GLIDER_SIZE; i++) {
        formTemplate[2][i] = true;
    }
}