#ifndef _FORM_HPP_
#define _FORM_HPP_

#include <vector>

class Form {
    private:
        std::pair<int, int> startingCoord;
    protected:
        std::vector< std::vector<bool> > formTemplate;
    public:
        Form(int x, int y);
        int getX();
        int getY();
        std::vector< std::vector<bool> > getTemplate(); 
        virtual void defineTemplate() = 0;
};

#endif