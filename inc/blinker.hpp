#ifndef _BLINKER_HPP_
#define _BLINKER_HPP_

#include "form.hpp"

class Blinker : public Form {
    public:
        Blinker(int x, int y);
        void defineTemplate();
};

#endif