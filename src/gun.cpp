#include "gun.hpp"

#define GUN_WIDTH 9
#define GUN_HEIGHT 36

using namespace std;

Gun::Gun(int x, int y) : Form(x, y) {
    formTemplate = vector< vector<bool> > (GUN_WIDTH, vector<bool> (GUN_HEIGHT, false));
    defineTemplate();
}

void Gun::defineTemplate() {
    formTemplate[0][24] = true;
    formTemplate[1][22] = true;
    formTemplate[1][24] = true;
    formTemplate[2][12] = true;
    formTemplate[2][13] = true;
    formTemplate[2][20] = true;
    formTemplate[2][21] = true;
    formTemplate[2][34] = true;
    formTemplate[2][35] = true;
    formTemplate[3][11] = true;
    formTemplate[3][15] = true;
    formTemplate[3][20] = true;
    formTemplate[3][21] = true;
    formTemplate[3][34] = true;
    formTemplate[3][35] = true;
    formTemplate[4][0] = true;
    formTemplate[4][1] = true;
    formTemplate[4][10] = true;
    formTemplate[4][16] = true;
    formTemplate[4][20] = true;
    formTemplate[4][21] = true;
    formTemplate[5][0] = true;
    formTemplate[5][1] = true;
    formTemplate[5][10] = true;
    formTemplate[5][14] = true;
    formTemplate[5][16] = true;
    formTemplate[5][17] = true;
    formTemplate[5][22] = true;
    formTemplate[5][24] = true;
    formTemplate[6][10] = true;
    formTemplate[6][16] = true;
    formTemplate[6][24] = true;
    formTemplate[7][11] = true;
    formTemplate[7][15] = true;
    formTemplate[8][12] = true;
    formTemplate[8][13] = true;
}